package com.example.employeecrud.model;

import jakarta.persistence.*;
import lombok.Data;

@Entity
@Data
@Table(name = "positions")
//Должности
public class Position {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "position_name")
    private String positionName;
}
