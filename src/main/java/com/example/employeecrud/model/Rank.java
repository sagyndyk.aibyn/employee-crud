package com.example.employeecrud.model;

import jakarta.persistence.*;
import lombok.Data;

@Entity
@Data
@Table(name = "ranks")
//Звания
public class Rank {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "rank_name")
    private String rankName;
}
