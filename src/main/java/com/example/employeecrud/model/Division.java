package com.example.employeecrud.model;

import jakarta.persistence.*;
import lombok.Data;

@Entity
@Data
@Table(name = "divisions")
//Подразделения
public class Division {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "division_name")
    private String divisionName;
}
