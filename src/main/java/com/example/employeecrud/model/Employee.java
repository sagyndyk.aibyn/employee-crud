package com.example.employeecrud.model;

import jakarta.persistence.*;
import lombok.Data;

import java.time.LocalDate;

@Entity
@Data
@Table(name = "employees")
//Сотрудники
public class Employee {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "lastname")
    private String lastname;

    @Column(name = "name")
    private String name;

    @Column(name = "patronymic")
    private String patronymic;

    @Column(name = "photo")
    private String photo;

    @Column(name = "birth_date")
    private LocalDate birthDate;

    @ManyToOne
    @JoinColumn(name = "position_id")
    private Position position;

    @ManyToOne
    @JoinColumn(name = "rank_id")
    private Rank rank;

    @ManyToOne
    @JoinColumn(name = "division_id")
    private Division division;

    @Column(name = "contract_date")
    private LocalDate contractDate;

    @Column(name = "contract_limit")
    private Long contractLimit;
}
