package com.example.employeecrud.controller;

import com.example.employeecrud.model.Employee;
import com.example.employeecrud.service.serviceImpl.unloading.PdfGenerator;
import com.example.employeecrud.service.serviceInterface.EmployeeService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.time.LocalDate;
import java.util.List;

@RestController
@RequestMapping("/api/v1/employee")
@Tag(name = "Сотрудник")
public class EmployeeController {

    private final EmployeeService employeeService;
    private final PdfGenerator pdfGenerator;

    public EmployeeController(EmployeeService employeeService, PdfGenerator pdfGenerator) {
        this.employeeService = employeeService;
        this.pdfGenerator = pdfGenerator;
    }

    @Operation(summary = "Получение данных о сотруднике по ID")
    @GetMapping("/{id}")
    public ResponseEntity<Employee> getById(@PathVariable(value = "id") Long id){
        Employee employee = this.employeeService.getByIdThrowException(id);
        return ResponseEntity.ok().body(employee);
    }

    @Operation(summary = "Получение данных о всех сотрудниках")
    @GetMapping("/")
    public List<Employee> getAll(){
        return this.employeeService.getAll();
    }

    @Operation(summary = "Получение данных о всех сотрудниках в виде PDF-файла")
    @GetMapping(value = "/get-pdf", produces = MediaType.APPLICATION_PDF_VALUE)
    public ResponseEntity<?> getPdfRoom() throws Exception {

        ByteArrayInputStream bis = pdfGenerator.createPdfEmployee();
        var headers = new HttpHeaders();
        headers.add("Content-Disposition", "inline; filename=Employees_" + LocalDate.now() +".pdf");

        return ResponseEntity
                .ok()
                .headers(headers)
                .contentType(MediaType.APPLICATION_PDF)
                .body(new InputStreamResource(bis));
    }

    @Operation(summary = "Создание нового сотрудника")
    @PostMapping("/create")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<HttpStatus> create(@RequestParam String lastname,
                                             @RequestParam String name,
                                             @RequestParam String patronymic,
                                             @RequestParam MultipartFile photo,
                                             @RequestParam LocalDate birthDate,
                                             @RequestParam Long position,
                                             @RequestParam Long rank,
                                             @RequestParam Long division,
                                             @RequestParam LocalDate contractDate,
                                             @RequestParam Long contractLimit)throws IOException {
        this.employeeService.create(lastname, name, patronymic, photo, birthDate, position, rank, division, contractDate, contractLimit);
        return new ResponseEntity<>(HttpStatus.OK);
    }
    //Из-за объекта MultipartFile я должен использовать метод @RequestParam а мог бы просто написать @RequestBody EmployeeDtoRequest.
    //Но пришлось использовать @RequestParam каждому параметру.

    @Operation(summary = "Редактирование сотрудника по ID")
    @PutMapping("/update/{id}")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<HttpStatus> update(@RequestParam String lastname,
                                             @RequestParam String name,
                                             @RequestParam String patronymic,
                                             @RequestParam MultipartFile photo,
                                             @RequestParam LocalDate birthDate,
                                             @RequestParam Long position,
                                             @RequestParam Long rank,
                                             @RequestParam Long division,
                                             @RequestParam LocalDate contractDate,
                                             @RequestParam Long contractLimit, @PathVariable(name = "id") Long id){
        this.employeeService.update(lastname, name, patronymic, photo, birthDate, position, rank, division, contractDate, contractLimit, id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @Operation(summary = "Удаление сотрудника по ID")
    @DeleteMapping("/delete/{id}")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<HttpStatus> delete(@PathVariable(name = "id") Long id){
        this.employeeService.delete(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
