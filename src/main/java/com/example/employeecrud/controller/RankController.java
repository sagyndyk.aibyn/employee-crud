package com.example.employeecrud.controller;

import com.example.employeecrud.dto.request.RankDtoRequest;
import com.example.employeecrud.model.Rank;
import com.example.employeecrud.service.serviceInterface.RankService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/rank")
@Tag(name = "Звание")
public class RankController {

    private final RankService rankService;

    public RankController(RankService rankService) {
        this.rankService = rankService;
    }

    @Operation(summary = "Получение данных о звании по ID")
    @GetMapping("/{id}")
    public ResponseEntity<Rank> getById(@PathVariable(value = "id") Long id){
        Rank rank = this.rankService.getByIdThrowException(id);
        return ResponseEntity.ok().body(rank);
    }

    @Operation(summary = "Получение данных о всех звяниях")
    @GetMapping("/")
    public List<Rank> getAll(){
        return this.rankService.getAll();
    }

    @Operation(summary = "Создание нового звания")
    @PostMapping("/create")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<HttpStatus> create(@RequestBody RankDtoRequest rankDtoRequest){
        this.rankService.create(rankDtoRequest);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @Operation(summary = "Редактирования звания по ID")
    @PutMapping("/update/{id}")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<HttpStatus> update(@RequestBody RankDtoRequest rankDtoRequest, @PathVariable(name = "id") Long id){
        this.rankService.update(rankDtoRequest, id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @Operation(summary = "Удаление звания по ID")
    @DeleteMapping("/delete/{id}")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<HttpStatus> delete(@PathVariable(name = "id") Long id){
        this.rankService.delete(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
