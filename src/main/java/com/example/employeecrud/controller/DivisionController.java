package com.example.employeecrud.controller;


import com.example.employeecrud.dto.request.DivisionDtoRequest;
import com.example.employeecrud.model.Division;
import com.example.employeecrud.service.serviceInterface.DivisionService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/division")
@Tag(name = "Подразделение")
public class DivisionController {

    private final DivisionService divisionService;

    public DivisionController(DivisionService divisionService) {
        this.divisionService = divisionService;
    }

    @Operation(summary = "Получение данных о подразделении по ID")
    @GetMapping("/{id}")
    public ResponseEntity<Division> getById(@PathVariable(value = "id") Long id){
        Division division = this.divisionService.getByIdThrowException(id);
        return ResponseEntity.ok().body(division);
    }

    @Operation(summary = "Получение данных о всех подразделениях")
    @GetMapping("/")
    public List<Division> getAll(){
        return this.divisionService.getAll();
    }

    @Operation(summary = "Создание нового подразделения")
    @PostMapping("/create")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<HttpStatus> create(@RequestBody DivisionDtoRequest divisionDtoRequest){
        this.divisionService.create(divisionDtoRequest);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @Operation(summary = "Редактирование сущестующего подразделения по ID")
    @PutMapping("/update/{id}")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<HttpStatus> update(@RequestBody DivisionDtoRequest divisionDtoRequest, @PathVariable(name = "id") Long id){
        this.divisionService.update(divisionDtoRequest, id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @Operation(summary = "Удаление подразделения по ID")
    @DeleteMapping("/delete/{id}")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<HttpStatus> delete(@PathVariable(name = "id") Long id){
        this.divisionService.delete(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

}
