package com.example.employeecrud.controller;

import com.example.employeecrud.dto.request.PositionDtoRequest;
import com.example.employeecrud.model.Position;
import com.example.employeecrud.service.serviceInterface.PositionService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/position")
@Tag(name = "Должность")
public class PositionController {
    private final PositionService positionService;

    public PositionController(PositionService positionService) {
        this.positionService = positionService;
    }

    @Operation(summary = "Получение данных о должности по ID")
    @GetMapping("/{id}")
    public ResponseEntity<Position> getById(@PathVariable(value = "id") Long id){
        Position position = this.positionService.getByIdThrowException(id);
        return ResponseEntity.ok().body(position);
    }

    @Operation(summary = "Получение данных о всех должностях")
    @GetMapping("/")
    public List<Position> getAll(){
        return this.positionService.getAll();
    }

    @Operation(summary = "Создание новой должности")
    @PostMapping("/create")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<HttpStatus> create(@RequestBody PositionDtoRequest positionDtoRequest){
        this.positionService.create(positionDtoRequest);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @Operation(summary = "Реадктирование должности по ID")
    @PutMapping("/update/{id}")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<HttpStatus> update(@RequestBody PositionDtoRequest positionDtoRequest, @PathVariable(name = "id") Long id){
        this.positionService.update(positionDtoRequest, id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @Operation(summary = "Удаление должности по ID")
    @DeleteMapping("/delete/{id}")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<HttpStatus> delete(@PathVariable(name = "id") Long id){
        this.positionService.delete(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
