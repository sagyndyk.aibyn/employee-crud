package com.example.employeecrud.service.serviceInterface;


import com.example.employeecrud.dto.request.PositionDtoRequest;
import com.example.employeecrud.model.Position;

import java.util.List;
import java.util.Optional;

public interface PositionService {
    Optional<Position> getById(Long id);

    Position getByIdThrowException(Long id);

    List<Position> getAll();

    void create(PositionDtoRequest positionDtoRequest);

    void update(PositionDtoRequest positionDtoRequest, Long id);

    void delete(Long id);
}
