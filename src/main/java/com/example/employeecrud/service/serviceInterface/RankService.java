package com.example.employeecrud.service.serviceInterface;

import com.example.employeecrud.dto.request.RankDtoRequest;
import com.example.employeecrud.model.Rank;

import java.util.List;
import java.util.Optional;

public interface RankService {

    Optional<Rank> getById(Long id);

    Rank getByIdThrowException(Long id);

    List<Rank> getAll();

    void create(RankDtoRequest rankDtoRequest);

    void update(RankDtoRequest rankDtoRequest, Long id);

    void delete(Long id);
}
