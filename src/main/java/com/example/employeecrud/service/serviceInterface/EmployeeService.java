package com.example.employeecrud.service.serviceInterface;


import com.example.employeecrud.dto.request.EmployeeDtoRequest;
import com.example.employeecrud.model.Employee;
import org.springframework.web.multipart.MultipartFile;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

public interface EmployeeService {
    Optional<Employee> getById(Long id);

    Employee getByIdThrowException(Long id);

    List<Employee> getAll();

    void create(String lastname, String name, String patronymic, MultipartFile photo, LocalDate birthDate, Long position, Long rank, Long division, LocalDate contractDate, Long contractLimit);

    void update(String lastname, String name, String patronymic, MultipartFile photo, LocalDate birthDate, Long position, Long rank, Long division, LocalDate contractDate, Long contractLimit, Long id);

    void delete(Long id);
}
