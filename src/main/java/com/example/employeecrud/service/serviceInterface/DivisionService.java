package com.example.employeecrud.service.serviceInterface;

import com.example.employeecrud.dto.request.DivisionDtoRequest;
import com.example.employeecrud.model.Division;

import java.util.List;
import java.util.Optional;

public interface DivisionService {
    Optional<Division> getById(Long id);

    Division getByIdThrowException(Long id);

    List<Division> getAll();

    void create(DivisionDtoRequest divisionDtoRequest);

    void update(DivisionDtoRequest divisionDtoRequest, Long id);

    void delete(Long id);
}
