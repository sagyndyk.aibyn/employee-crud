package com.example.employeecrud.service.serviceImpl;

import com.example.employeecrud.dto.request.DivisionDtoRequest;
import com.example.employeecrud.exception.CustomNotFoundException;
import com.example.employeecrud.exception.ExceptionDescription;
import com.example.employeecrud.exception.RepositoryException;
import com.example.employeecrud.model.Division;
import com.example.employeecrud.repository.DivisionRepository;
import com.example.employeecrud.service.serviceInterface.DivisionService;
import org.apache.logging.log4j.util.Strings;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class DivisionServiceImpl implements DivisionService {

    private final DivisionRepository divisionRepository;

    public DivisionServiceImpl(DivisionRepository divisionRepository) {
        this.divisionRepository = divisionRepository;
    }

    @Override
    public Optional<Division> getById(Long id) {
        return this.divisionRepository.findById(id);
    }

    @Override
    public Division getByIdThrowException(Long id) {
        return this.getById(id)
                .orElseThrow(() -> new CustomNotFoundException
                        (String.format(ExceptionDescription.CustomNotFoundException, "Division", "id", id)));
    }

    @Override
    public List<Division> getAll() {
        return this.divisionRepository.findAll();
    }

    @Override
    public void create(DivisionDtoRequest divisionDtoRequest) {
        Division division = new Division();

        division.setDivisionName(divisionDtoRequest.getDivisionName());

        try{
            this.divisionRepository.save(division);
        }catch (Exception e){
            throw new RepositoryException(String.format(ExceptionDescription.RepositoryException, "creating", "division"));
        }
    }

    @Override
    public void update(DivisionDtoRequest divisionDtoRequest, Long id) {
        Division division = this.getByIdThrowException(id);

        if(Strings.isNotBlank(divisionDtoRequest.getDivisionName())) division.setDivisionName(divisionDtoRequest.getDivisionName());

        try{
            this.divisionRepository.save(division);
        }catch (Exception e){
            throw new RepositoryException(String.format(ExceptionDescription.RepositoryException, "updating", "division"));
        }
    }

    @Override
    public void delete(Long id) {
        Division division = this.getByIdThrowException(id);

        try{
            this.divisionRepository.delete(division);
        }catch (Exception e){
            throw new RepositoryException(String.format(ExceptionDescription.RepositoryException, "deleting", "division"));
        }
    }
}
