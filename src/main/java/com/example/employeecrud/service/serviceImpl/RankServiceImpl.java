package com.example.employeecrud.service.serviceImpl;

import com.example.employeecrud.dto.request.RankDtoRequest;
import com.example.employeecrud.exception.CustomNotFoundException;
import com.example.employeecrud.exception.ExceptionDescription;
import com.example.employeecrud.exception.RepositoryException;
import com.example.employeecrud.model.Rank;
import com.example.employeecrud.repository.RankRepository;
import com.example.employeecrud.service.serviceInterface.RankService;
import org.apache.logging.log4j.util.Strings;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class RankServiceImpl implements RankService {
    private final RankRepository rankRepository;

    public RankServiceImpl(RankRepository rankRepository) {
        this.rankRepository = rankRepository;
    }

    @Override
    public Optional<Rank> getById(Long id) {
        return this.rankRepository.findById(id);
    }

    @Override
    public Rank getByIdThrowException(Long id) {
        return this.getById(id)
                .orElseThrow(() -> new CustomNotFoundException
                        (String.format(ExceptionDescription.CustomNotFoundException, "Rank", "id", id)));
    }

    @Override
    public List<Rank> getAll() {
        return this.rankRepository.findAll();
    }

    @Override
    public void create(RankDtoRequest rankDtoRequest) {
        Rank rank = new Rank();

        rank.setRankName(rankDtoRequest.getRankName());

        try{
            this.rankRepository.save(rank);
        }catch (Exception e){
            throw new RepositoryException(String.format(ExceptionDescription.RepositoryException, "creating", "rank"));
        }
    }

    @Override
    public void update(RankDtoRequest rankDtoRequest, Long id) {
        Rank rank = this.getByIdThrowException(id);

        if(Strings.isNotBlank(rankDtoRequest.getRankName())) rank.setRankName(rankDtoRequest.getRankName());

        try{
            this.rankRepository.save(rank);
        }catch (Exception e){
            throw new RepositoryException(String.format(ExceptionDescription.RepositoryException, "updating", "rank"));
        }
    }

    @Override
    public void delete(Long id) {
        Rank rank = this.getByIdThrowException(id);

        try{
            this.rankRepository.delete(rank);
        }catch (Exception e){
            throw new RepositoryException(String.format(ExceptionDescription.RepositoryException, "deleting", "rank"));
        }
    }
}
