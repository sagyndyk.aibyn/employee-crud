package com.example.employeecrud.service.serviceImpl;

import com.example.employeecrud.dto.request.EmployeeDtoRequest;
import com.example.employeecrud.exception.CustomNotFoundException;
import com.example.employeecrud.exception.ExceptionDescription;
import com.example.employeecrud.exception.RepositoryException;
import com.example.employeecrud.model.Employee;
import com.example.employeecrud.repository.EmployeeRepository;
import com.example.employeecrud.service.serviceInterface.DivisionService;
import com.example.employeecrud.service.serviceInterface.EmployeeService;
import com.example.employeecrud.service.serviceInterface.PositionService;
import com.example.employeecrud.service.serviceInterface.RankService;
import org.apache.logging.log4j.util.Strings;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.time.LocalDate;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Service
public class EmployeeServiceImpl implements EmployeeService {

    @Value("${file.path}")
    private String filePath;
    private final EmployeeRepository employeeRepository;
    private final PositionService positionService;
    private final DivisionService divisionService;
    private final RankService rankService;

    public EmployeeServiceImpl(EmployeeRepository employeeRepository, PositionService positionService, DivisionService divisionService, RankService rankService) {
        this.employeeRepository = employeeRepository;
        this.positionService = positionService;
        this.divisionService = divisionService;
        this.rankService = rankService;
    }

    @Override
    public Optional<Employee> getById(Long id) {
        return this.employeeRepository.findById(id);
    }

    @Override
    public Employee getByIdThrowException(Long id) {
        return this.getById(id)
                .orElseThrow(() -> new CustomNotFoundException
                        (String.format(ExceptionDescription.CustomNotFoundException, "Employee", "id", id)));
    }

    @Override
    public List<Employee> getAll() {
        return this.employeeRepository.findAll();
    }

    @Override
    public void create(String lastname, String name, String patronymic, MultipartFile photo, LocalDate birthDate,
                       Long position, Long rank, Long division, LocalDate contractDate, Long contractLimit) {
        Employee employee = new Employee();

        employee.setLastname(lastname);
        employee.setName(name);
        employee.setPatronymic(patronymic);
        employee.setPhoto(photo.getOriginalFilename());
        try {
            photo.transferTo(new File(System.getProperty("user.dir") + "/" + filePath + "/" + photo.getOriginalFilename()));
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
        employee.setBirthDate(birthDate);
        employee.setPosition(this.positionService.getByIdThrowException(position));
        employee.setRank(this.rankService.getByIdThrowException(rank));
        employee.setDivision(this.divisionService.getByIdThrowException(division));
        employee.setContractDate(contractDate);
        employee.setContractLimit(contractLimit);

        try{
            this.employeeRepository.save(employee);
        }catch (Exception e){
            throw new RepositoryException(String.format(ExceptionDescription.RepositoryException, "creating", "employee"));
        }
    }

    @Override
    public void update(String lastname, String name, String patronymic, MultipartFile photo, LocalDate birthDate,
                       Long position, Long rank, Long division, LocalDate contractDate, Long contractLimit, Long id) {
        Employee employee = this.getByIdThrowException(id);

        if(Strings.isNotBlank(lastname)) employee.setLastname(lastname);
        if(Strings.isNotBlank(name)) employee.setName(name);
        if(Strings.isNotBlank(patronymic)) employee.setPatronymic(patronymic);
        if(Strings.isNotBlank(photo.getOriginalFilename())) employee.setPhoto(photo.getOriginalFilename());
        if(Objects.nonNull(birthDate)) employee.setBirthDate(birthDate);
        if(Objects.nonNull(position)) employee.setPosition(this.positionService.getByIdThrowException(position));
        if(Objects.nonNull(rank)) employee.setRank(this.rankService.getByIdThrowException(rank));
        if(Objects.nonNull(division)) employee.setDivision(this.divisionService.getByIdThrowException(division));
        if(Objects.nonNull(contractDate)) employee.setContractDate(contractDate);
        if(Objects.nonNull(contractLimit)) employee.setContractLimit(contractLimit);

        try{
            this.employeeRepository.save(employee);
        }catch (Exception e){
            throw new RepositoryException(String.format(ExceptionDescription.RepositoryException, "updating", "employee"));
        }
    }

    @Override
    public void delete(Long id) {
        Employee employee = this.getByIdThrowException(id);

        try{
            this.employeeRepository.delete(employee);
        }catch (Exception e){
            throw new RepositoryException(String.format(ExceptionDescription.RepositoryException, "deleting", "employee"));
        }
    }
}
