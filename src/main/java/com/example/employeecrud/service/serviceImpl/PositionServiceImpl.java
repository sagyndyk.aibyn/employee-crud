package com.example.employeecrud.service.serviceImpl;


import com.example.employeecrud.dto.request.PositionDtoRequest;
import com.example.employeecrud.exception.CustomNotFoundException;
import com.example.employeecrud.exception.ExceptionDescription;
import com.example.employeecrud.exception.RepositoryException;
import com.example.employeecrud.model.Position;
import com.example.employeecrud.repository.PositionRepository;
import com.example.employeecrud.service.serviceInterface.PositionService;
import org.apache.logging.log4j.util.Strings;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class PositionServiceImpl implements PositionService {

    private final PositionRepository positionRepository;

    public PositionServiceImpl(PositionRepository positionRepository) {
        this.positionRepository = positionRepository;
    }

    @Override
    public Optional<Position> getById(Long id) {
        return this.positionRepository.findById(id);
    }

    @Override
    public Position getByIdThrowException(Long id) {
        return this.getById(id)
                .orElseThrow(() -> new CustomNotFoundException
                        (String.format(ExceptionDescription.CustomNotFoundException, "Position", "id", id)));
    }

    @Override
    public List<Position> getAll() {
        return this.positionRepository.findAll();
    }

    @Override
    public void create(PositionDtoRequest positionDtoRequest) {
        Position position = new Position();

        position.setPositionName(positionDtoRequest.getPositionName());

        try{
            this.positionRepository.save(position);
        }catch (Exception e){
            throw new RepositoryException(String.format(ExceptionDescription.RepositoryException, "creating", "position"));
        }
    }

    @Override
    public void update(PositionDtoRequest positionDtoRequest, Long id) {
        Position position = this.getByIdThrowException(id);

        if(Strings.isNotBlank(positionDtoRequest.getPositionName())) position.setPositionName(positionDtoRequest.getPositionName());

        try{
            this.positionRepository.save(position);
        }catch (Exception e){
            throw new RepositoryException(String.format(ExceptionDescription.RepositoryException, "updating", "position"));
        }
    }

    @Override
    public void delete(Long id) {
        Position position = this.getByIdThrowException(id);

        try{
            this.positionRepository.delete(position);
        }catch (Exception e){
            throw new RepositoryException(String.format(ExceptionDescription.RepositoryException, "deleting", "position"));
        }
    }
}
