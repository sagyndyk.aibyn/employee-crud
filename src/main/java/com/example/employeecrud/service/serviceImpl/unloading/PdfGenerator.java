package com.example.employeecrud.service.serviceImpl.unloading;

import com.example.employeecrud.model.Employee;
import com.example.employeecrud.service.serviceInterface.EmployeeService;
import com.itextpdf.text.*;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import org.springframework.stereotype.Service;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.stream.Stream;

@Service
public class PdfGenerator {

    private final EmployeeService employeeService;
    public PdfGenerator(EmployeeService employeeService) {
        this.employeeService = employeeService;
    }

    public ByteArrayInputStream createPdfEmployee() {

        Document document = new Document();
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        try {
            List<Employee> data = employeeService.getAll();

            PdfWriter.getInstance(document, out);
            document.open();

            int sizes[] = {15, 15, 20, 20, 20, 15, 15, 20, 15};
            PdfPTable table = new PdfPTable(9);
            table.setWidthPercentage(100);
            table.setWidths(sizes);
            addTableHeader(table);

            BaseFont baseFont = BaseFont.createFont("c:/windows/fonts/times.ttf", "cp1251", BaseFont.EMBEDDED);
            Font font = new Font(baseFont, 7);
            for(Employee employee : data){
                table.addCell(new Phrase(employee.getLastname() + " " + employee.getName() + " " + employee.getPatronymic(), font));
                table.addCell(new Phrase(employee.getBirthDate().toString(), font));
                table.addCell(new Phrase(employee.getPosition().getPositionName(), font));
                table.addCell(new Phrase(employee.getRank().getRankName(), font));
                table.addCell(new Phrase(employee.getDivision().getDivisionName(), font));
                table.addCell(new Phrase(employee.getContractDate().toString(), font));
                table.addCell(new Phrase(employee.getContractLimit().toString(), font));
                table.addCell(new Phrase(employee.getContractDate().plusYears(employee.getContractLimit()).toString(), font));
                table.addCell(new Phrase(employee.getPhoto(), font));
            }



            document.add(getDocumentHeader());
            document.add(table);
            document.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return new ByteArrayInputStream(out.toByteArray());
    }

    private void addTableHeader(PdfPTable table) throws IOException, DocumentException {

        BaseFont baseFont = BaseFont.createFont("c:/windows/fonts/times.ttf","cp1251",BaseFont.EMBEDDED);
        Stream.of("ФИО", "Дата рождения", "Должность", "Звания", "Подразделение", "Дата заключения контракта", "Срок контракта (в годах)", "Дата завершения контракта", "Фото")
                .forEach(columnTitle -> {
                    PdfPCell header = new PdfPCell();
                    header.setBackgroundColor(BaseColor.LIGHT_GRAY);
                    header.setBorderWidth(1);
                    header.setPhrase(new Phrase(columnTitle, new Font(baseFont, 9)));
                    table.addCell(header);
                });
    }

    private Paragraph getDocumentHeader() throws DocumentException, IOException {
        BaseFont baseFont = BaseFont.createFont("c:/windows/fonts/times.ttf","cp1251",BaseFont.EMBEDDED);
        String documentHeader = "\"Служба Государственной Охраны РК\" \nработа кандидата Сагындык А.Б.\t\n\"__\"__________________2024.\n\n";
        Paragraph paragraph = new Paragraph(documentHeader, new Font(baseFont, 7));
        paragraph.setAlignment(Element.ALIGN_RIGHT);
        return paragraph;
    }

    private Paragraph getDocumentFooter() throws DocumentException, IOException {
        BaseFont baseFont = BaseFont.createFont("c:/windows/fonts/times.ttf","cp1251",BaseFont.EMBEDDED);
        String documentFooter = "г. Астана";
        Paragraph paragraph = new Paragraph(documentFooter, new Font(baseFont, 7));
        paragraph.setAlignment(Element.ALIGN_CENTER);
        return paragraph;
    }
}
