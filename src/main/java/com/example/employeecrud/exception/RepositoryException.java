package com.example.employeecrud.exception;

public class RepositoryException extends RuntimeException {

    public RepositoryException(String message){
        super(message);
    }
}
