package com.example.employeecrud.repository;

import com.example.employeecrud.model.Division;
import com.example.employeecrud.model.Employee;
import com.example.employeecrud.model.Position;
import com.example.employeecrud.model.Rank;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface EmployeeRepository extends JpaRepository<Employee, Long> {

    Optional<Employee> findByName(String name);
    Optional<Employee> findByLastname(String lastname);
    Optional<Employee> findByPatronymic(String patronymic);
    Optional<Employee> findByPosition(Position position);
    Optional<Employee> findByRank(Rank rank);
    Optional<Employee> findByDivision(Division division);

}
