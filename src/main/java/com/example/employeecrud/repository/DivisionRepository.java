package com.example.employeecrud.repository;

import com.example.employeecrud.model.Division;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface DivisionRepository extends JpaRepository<Division, Long> {

    Optional<Division> findByDivisionName(String divisionName);
}
