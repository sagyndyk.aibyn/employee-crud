package com.example.employeecrud.dto.response;

import lombok.Data;

import java.time.LocalDate;


@Data
public class EmployeeDtoResponse {

    private Long id;
    private String lastname;
    private String name;
    private String patronymic;
    private String photo;
    private LocalDate birthDate;
    private Long position;
    private Long rank;
    private Long division;
    private LocalDate contractDate;
    private Long contractLimit;
}
