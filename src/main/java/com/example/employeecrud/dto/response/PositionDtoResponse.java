package com.example.employeecrud.dto.response;

import lombok.Data;

@Data
public class PositionDtoResponse {

    private Long id;
    private String positionName;
}
