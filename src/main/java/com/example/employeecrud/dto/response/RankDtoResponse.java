package com.example.employeecrud.dto.response;

import lombok.Data;

@Data
public class RankDtoResponse {

    private Long id;
    private String rankName;
}
