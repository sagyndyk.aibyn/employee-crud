package com.example.employeecrud.dto.response;

import lombok.Data;

@Data
public class DivisionDtoResponse {

    private Long id;
    private String divisionName;
}
