package com.example.employeecrud.dto.request;

import lombok.Data;

@Data
public class DivisionDtoRequest {

    private String divisionName;
}
