package com.example.employeecrud.dto.request;

import lombok.Data;

@Data
public class PositionDtoRequest {

    private String positionName;
}
