package com.example.employeecrud.dto.request;

import lombok.Data;
import org.springframework.web.multipart.MultipartFile;

import java.time.LocalDate;

@Data
public class EmployeeDtoRequest {

    private String lastname;
    private String name;
    private String patronymic;
    private MultipartFile photo;
    private LocalDate birthDate;
    private Long position;
    private Long rank;
    private Long division;
    private LocalDate contractDate;
    private Long contractLimit;
}
