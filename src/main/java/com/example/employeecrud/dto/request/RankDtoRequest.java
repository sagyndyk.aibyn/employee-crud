package com.example.employeecrud.dto.request;

import lombok.Data;

@Data
public class RankDtoRequest {

    private String rankName;
}
